FROM jboss/bpms:6.4.2

ADD kie-server-kafka-extension/target/kie-server-kafka-extension-1.0-kie-extension.tar.gz $JBOSS_HOME/standalone/deployments/kie-server.war/WEB-INF/lib/

COPY settings.xml /opt/jboss/.m2/settings.xml
USER root
RUN chown -R jboss:jboss /opt/jboss/.m2
USER jboss

#Launch configuration
WORKDIR $JBOSS_HOME
ENV LAUNCH_JBOSS_IN_BACKGROUND true
ENTRYPOINT ["bin/standalone.sh", "-c"]
CMD ["standalone.xml"]